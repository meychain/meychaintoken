const {
  BN,
  constants,
  expectEvent,
  expectRevert,
} = require("@openzeppelin/test-helpers");

const { expect } = require("chai");

const {
  increaseTime,
  duration,
  increaseTimeTo,
  latestTime,
} = require("./utils/adjustTime");
const BEP20Token = artifacts.require("MeyToken");
const LockToken = artifacts.require("Timelock");
const name = "Token";
const symbol = "TKN";
const initialSupply = new BN("2300000000000000000");
const decimals = "9";
const lockAmount = new BN("1000000000000000000000000");

contract(
  "LockToken",
  function ([_, owner, sender, beneficiary, anotherAccount]) {
    const amount = new BN(1000000000000);

    beforeEach(async function () {
      this.token = await BEP20Token.new(owner, {
        from: owner,
      });
      this.latestBlock = await web3.eth.getBlock("latest");
      this.start = await this.latestBlock.timestamp;

      this.lock = await LockToken.new(
        this.start + duration.days(180),
        beneficiary,
        { from: owner }
      );
    });
    describe("1. Deposit", function () {
      describe("Should be able to deposit", function () {
        it("Deposit successfully", async function () {
          await this.token.approve(
            this.lock.address,
            new BN("100000000000000"),
            {
              from: owner,
            }
          );
          const { logs } = await this.lock.deposit(
            this.token.address,
            new BN("100000000000000"),
            {
              from: owner,
            }
          );
          expectEvent.inLogs(logs, "TokenLocked", {});
        });
        it("Deposit failed", async function () {
          await this.token.approve(this.lock.address, new BN("10000000000"), {
            from: owner,
          });
          await expectRevert(
            this.lock.deposit(this.token.address, new BN("100000000000000"), {
              from: owner,
            }),
            `Transfer amount exceeds allowance -- Reason given: Transfer amount exceeds allowance.`
          );
        });
      });

    });
    describe("2. Withdraw", function () {
      describe("Should be able to withdraw", function () {
        beforeEach(async function () {
          await this.token.approve(
            this.lock.address,
            new BN("100000000000000"),
            {
              from: owner,
            }
          );
          await this.lock.deposit(
            this.token.address,
            new BN("100000000000000"),
            {
              from: owner,
            }
          );
        });
        it("Withdraw successfully", async function () {
          await increaseTimeTo(this.start + duration.days(180));
          await expect(
            this.lock.withdraw(this.token.address, new BN("100000000000000"), {
              from: owner,
            })
          );
        });
        it("Withdraw failed", async function () {
          await increaseTimeTo(this.start + duration.days(179));
          await expectRevert(
            this.lock.withdraw(this.token.address, new BN("100000000000000"), {
              from: owner,
            }),
            `LockToken: Withdraw too early -- Reason given: LockToken: Withdraw too early.`
          );
        });
        it("Withdraw failed from not owner", async function () {
          await increaseTimeTo(this.start + duration.days(179));
          await expectRevert(
            this.lock.withdraw(this.token.address, new BN("100000000000000"), {
              from: anotherAccount,
            }),
            `Ownable: caller is not the owner -- Reason given: Ownable: caller is not the owner.`
          );
        });
      });
    });
    describe("3. Increase Time", function () {
      describe("Should be able to increaseTime", function () {
        beforeEach(async function () {
          await this.token.approve(
            this.lock.address,
            new BN("100000000000000"),
            {
              from: owner,
            }
          );
          await this.lock.deposit(
            this.token.address,
            new BN("100000000000000"),
            {
              from: owner,
            }
          );
        });
        it("Increase time successfully", async function () {
          await expect(
            this.lock.increaseTimeLock(duration.days(10), {
              from: owner,
            })
          );
          await increaseTimeTo(this.start + duration.days(180));
          await expectRevert(
            this.lock.withdraw(this.token.address, new BN("100000000000000"), {
              from: owner,
            }),
            `LockToken: Withdraw too early -- Reason given: LockToken: Withdraw too early.`
          );
          await increaseTimeTo(this.start + duration.days(190));
          await expect(
            this.lock.withdraw(this.token.address, new BN("100000000000000"), {
              from: owner,
            })
          );
        });
        it("Increase time from not owner", async function () {
          await expectRevert(
            this.lock.increaseTimeLock(duration.days(10), {
              from: anotherAccount,
            }),
            `Ownable: caller is not the owner -- Reason given: Ownable: caller is not the owner.`
          );
        });
      });
    });
  }
);
