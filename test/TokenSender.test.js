const {
  BN,
  constants,
  expectEvent,
  expectRevert,
} = require("@openzeppelin/test-helpers");
const { ZERO_ADDRESS } = constants;
const { expect } = require("chai");
const { shouldBehaveLikeBEP20 } = require("./BEP20.behavior");

const TokenSender = artifacts.require("TokenSender");
const BEP20Token = artifacts.require("MeyToken");

contract("TokenSender", function (accounts) {
  const [deployer, anotherAccount, anotherAccount2] = accounts;
  const initialSupply = new BN("2300000000000000000");
  beforeEach(async function () {
    this.token = await BEP20Token.new('0x0000000000000000000000000000000000000000', {
      from: deployer,
    });
    this.sender = await TokenSender.new({ from: deployer });
  });
  describe("sendTokens", function () {
    it("when it has enough balance", async function () {
      await this.token.transfer(this.sender.address, initialSupply, {
        from: deployer,
      });
        await this.sender.sendTokens(
          this.token.address,
          [[new BN("100000"), anotherAccount],
          [new BN("100001"), anotherAccount2 ]]
        );
        expect(
          await this.token.balanceOf(anotherAccount)
        ).to.be.bignumber.equal(new BN("100000"));
        expect(
          await this.token.balanceOf(anotherAccount2)
        ).to.be.bignumber.equal(new BN("100001"));
        expect(
          await this.token.balanceOf(this.sender.address)
        ).to.be.bignumber.equal(new BN("2299999999999799999"));
    });
    it("when it doesnt have enough balance", async function () {
      await this.token.transfer(this.sender.address, initialSupply, {
        from: deployer,
      });
      await expectRevert(
        this.sender.sendTokens(
          this.token.address,
          [[initialSupply + 1,anotherAccount]]
        ),
        `TokenSender: Insufficient funds.`
      );
    });
    it("when input array empty", async function () {
      await expectRevert(
        this.sender.sendTokens(this.token.address, []),
        `TokenSender: Input array must not be empty`
      );
    });
    // it("when input length mismatch", async function () {
    //   await expectRevert(
    //     this.sender.sendTokens(this.token.address, [], [initialSupply]),
    //     `TokenSender: Input length mismatch.`
    //   );
    // });
  });
  describe("balanceOf", function () {
    it("should display balance of contract wallet in requested token", async function () {
      await this.token.transfer(this.sender.address, initialSupply, {
        from: deployer,
      });
      expect(
        await this.sender.balanceOf(this.token.address)
      ).to.be.bignumber.equal(initialSupply);
    });
    it("should display zero when it not funded", async function () {
      expect(
        await this.sender.balanceOf(this.token.address)
      ).to.be.bignumber.equal(new BN(0));
    });
  });
});
