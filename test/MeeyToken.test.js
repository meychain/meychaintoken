const {
  BN,
  constants,
  expectEvent,
  expectRevert,
} = require("@openzeppelin/test-helpers");
const { ZERO_ADDRESS } = constants;

const { expect } = require("chai");
const {
  shouldBehaveLikeBEP20,
  shouldBehaveLikeBEP20Transfer,
  shouldBehaveLikeBEP20Approve,
} = require("./BEP20.behavior");

const MeyToken = artifacts.require("MeyToken");

contract("MeyToken", function (accounts) {
  const [
    deployer,
    owner,
    initialHolder,
    recipient,
    anotherAccount,
    adminAddress,
  ] = accounts;

  const name = "MEY Token";
  const symbol = "MEY";
  const initialSupply = new BN("2300000000000000000");
  const decimals = "9";
  beforeEach(async function () {
    this.token = await MeyToken.new(adminAddress, { from: deployer });
  });

  it("deployer has the balance equal to initial supply", async function () {
    expect(await this.token.balanceOf(deployer)).to.be.bignumber.equal(
      initialSupply
    );
  });

  it("total supply is equal to initial supply", async function () {
    expect(await this.token.totalSupply()).to.be.bignumber.equal(initialSupply);
  });

  it("has correct name", async function () {
    expect(await this.token.name()).to.equal(name);
  });

  it("has correct symbol", async function () {
    expect(await this.token.symbol()).to.equal(symbol);
  });

  it("has correct decimals", async function () {
    expect(await this.token.decimals()).to.be.bignumber.equal(decimals);
  });
  describe("decrease allowance", function () {
    describe("when the spender is not the zero address", function () {
      const spender = recipient;

      function shouldDecreaseApproval(amount) {
        describe("when there was no approved amount before", function () {
          it("reverts", async function () {
            await expectRevert(
              this.token.decreaseAllowance(spender, amount, {
                from: initialHolder,
              }),
              "Decreased allowance below zero -- Reason given: Decreased allowance below zero."
            );
          });
        });

        describe("when the spender had an approved amount", function () {
          const approvedAmount = amount;

          beforeEach(async function () {
            ({ logs: this.logs } = await this.token.approve(
              spender,
              approvedAmount,
              { from: initialHolder }
            ));
          });

          it("emits an approval event", async function () {
            const { logs } = await this.token.decreaseAllowance(
              spender,
              approvedAmount,
              { from: initialHolder }
            );

            expectEvent.inLogs(logs, "Approval", {
              owner: initialHolder,
              spender: spender,
              value: new BN(0),
            });
          });

          it("decreases the spender allowance subtracting the requested amount", async function () {
            await this.token.decreaseAllowance(
              spender,
              approvedAmount.subn(1),
              { from: initialHolder }
            );

            expect(
              await this.token.allowance(initialHolder, spender)
            ).to.be.bignumber.equal("1");
          });

          it("sets the allowance to zero when all allowance is removed", async function () {
            await this.token.decreaseAllowance(spender, approvedAmount, {
              from: initialHolder,
            });
            expect(
              await this.token.allowance(initialHolder, spender)
            ).to.be.bignumber.equal("0");
          });

          it("reverts when more than the full allowance is removed", async function () {
            await expectRevert(
              this.token.decreaseAllowance(spender, approvedAmount.addn(1), {
                from: initialHolder,
              }),
              "Decreased allowance below zero -- Reason given: Decreased allowance below zero."
            );
          });
        });
      }

      describe("when the sender has enough balance", function () {
        const amount = initialSupply;

        shouldDecreaseApproval(amount);
      });

      describe("when the sender does not have enough balance", function () {
        const amount = initialSupply.addn(1);

        shouldDecreaseApproval(amount);
      });
    });

    describe("when the spender is the zero address", function () {
      const amount = initialSupply;
      const spender = ZERO_ADDRESS;

      it("reverts", async function () {
        await expectRevert(
          this.token.decreaseAllowance(spender, amount, {
            from: initialHolder,
          }),
          "Decreased allowance below zero -- Reason given: Decreased allowance below zero."
        );
      });
    });
  });

  describe("increase allowance", function () {
    const amount = initialSupply;

    describe("when the spender is not the zero address", function () {
      const spender = recipient;

      describe("when the sender has enough balance", function () {
        it("emits an approval event", async function () {
          const { logs } = await this.token.increaseAllowance(spender, amount, {
            from: initialHolder,
          });

          expectEvent.inLogs(logs, "Approval", {
            owner: initialHolder,
            spender: spender,
            value: amount,
          });
        });

        describe("when there was no approved amount before", function () {
          it("approves the requested amount", async function () {
            await this.token.increaseAllowance(spender, amount, {
              from: initialHolder,
            });

            expect(
              await this.token.allowance(initialHolder, spender)
            ).to.be.bignumber.equal(amount);
          });
        });

        describe("when the spender had an approved amount", function () {
          beforeEach(async function () {
            await this.token.approve(spender, new BN(1), {
              from: initialHolder,
            });
          });

          it("increases the spender allowance adding the requested amount", async function () {
            await this.token.increaseAllowance(spender, amount, {
              from: initialHolder,
            });

            expect(
              await this.token.allowance(initialHolder, spender)
            ).to.be.bignumber.equal(amount.addn(1));
          });
        });
      });

      describe("when the sender does not have enough balance", function () {
        const amount = initialSupply.addn(1);

        it("emits an approval event", async function () {
          const { logs } = await this.token.increaseAllowance(spender, amount, {
            from: initialHolder,
          });

          expectEvent.inLogs(logs, "Approval", {
            owner: initialHolder,
            spender: spender,
            value: amount,
          });
        });

        describe("when there was no approved amount before", function () {
          it("approves the requested amount", async function () {
            await this.token.increaseAllowance(spender, amount, {
              from: initialHolder,
            });

            expect(
              await this.token.allowance(initialHolder, spender)
            ).to.be.bignumber.equal(amount);
          });
        });

        describe("when the spender had an approved amount", function () {
          beforeEach(async function () {
            await this.token.approve(spender, new BN(1), {
              from: initialHolder,
            });
          });

          it("increases the spender allowance adding the requested amount", async function () {
            await this.token.increaseAllowance(spender, amount, {
              from: initialHolder,
            });

            expect(
              await this.token.allowance(initialHolder, spender)
            ).to.be.bignumber.equal(amount.addn(1));
          });
        });
      });
    });

    describe("when the spender is the zero address", function () {
      const spender = ZERO_ADDRESS;

      it("reverts", async function () {
        await expectRevert(
          this.token.increaseAllowance(spender, amount, {
            from: initialHolder,
          }),
          "Approve to the zero address -- Reason given: Approve to the zero address."
        );
      });
    });
  });

  describe("decrease allowance", function () {
    describe("when the spender is not the zero address", function () {
      const spender = recipient;

      function shouldDecreaseApproval(amount) {
        describe("when there was no approved amount before", function () {
          it("reverts", async function () {
            await expectRevert(
              this.token.decreaseAllowance(spender, amount, {
                from: initialHolder,
              }),
              "Decreased allowance below zero"
            );
          });
        });

        describe("when the spender had an approved amount", function () {
          const approvedAmount = amount;

          beforeEach(async function () {
            ({ logs: this.logs } = await this.token.approve(
              spender,
              approvedAmount,
              { from: initialHolder }
            ));
          });

          it("emits an approval event", async function () {
            const { logs } = await this.token.decreaseAllowance(
              spender,
              approvedAmount,
              { from: initialHolder }
            );

            expectEvent.inLogs(logs, "Approval", {
              owner: initialHolder,
              spender: spender,
              value: new BN(0),
            });
          });

          it("decreases the spender allowance subtracting the requested amount", async function () {
            await this.token.decreaseAllowance(
              spender,
              approvedAmount.subn(1),
              { from: initialHolder }
            );

            expect(
              await this.token.allowance(initialHolder, spender)
            ).to.be.bignumber.equal("1");
          });

          it("sets the allowance to zero when all allowance is removed", async function () {
            await this.token.decreaseAllowance(spender, approvedAmount, {
              from: initialHolder,
            });
            expect(
              await this.token.allowance(initialHolder, spender)
            ).to.be.bignumber.equal("0");
          });

          it("reverts when more than the full allowance is removed", async function () {
            await expectRevert(
              this.token.decreaseAllowance(spender, approvedAmount.addn(1), {
                from: initialHolder,
              }),
              "Decreased allowance below zero -- Reason given: Decreased allowance below zero."
            );
          });
        });
      }

      describe("when the sender has enough balance", function () {
        const amount = initialSupply;

        shouldDecreaseApproval(amount);
      });

      describe("when the sender does not have enough balance", function () {
        const amount = initialSupply.addn(1);

        shouldDecreaseApproval(amount);
      });
    });

    describe("when the spender is the zero address", function () {
      const amount = initialSupply;
      const spender = ZERO_ADDRESS;

      it("reverts", async function () {
        await expectRevert(
          this.token.decreaseAllowance(spender, amount, {
            from: initialHolder,
          }),
          "Decreased allowance below zero -- Reason given: Decreased allowance below zero."
        );
      });
    });
  });

  describe("increase allowance", function () {
    const amount = initialSupply;

    describe("when the spender is not the zero address", function () {
      const spender = recipient;

      describe("when the sender has enough balance", function () {
        it("emits an approval event", async function () {
          const { logs } = await this.token.increaseAllowance(spender, amount, {
            from: initialHolder,
          });

          expectEvent.inLogs(logs, "Approval", {
            owner: initialHolder,
            spender: spender,
            value: amount,
          });
        });

        describe("when there was no approved amount before", function () {
          it("approves the requested amount", async function () {
            await this.token.increaseAllowance(spender, amount, {
              from: initialHolder,
            });

            expect(
              await this.token.allowance(initialHolder, spender)
            ).to.be.bignumber.equal(amount);
          });
        });

        describe("when the spender had an approved amount", function () {
          beforeEach(async function () {
            await this.token.approve(spender, new BN(1), {
              from: initialHolder,
            });
          });

          it("increases the spender allowance adding the requested amount", async function () {
            await this.token.increaseAllowance(spender, amount, {
              from: initialHolder,
            });

            expect(
              await this.token.allowance(initialHolder, spender)
            ).to.be.bignumber.equal(amount.addn(1));
          });
        });
      });

      describe("when the sender does not have enough balance", function () {
        const amount = initialSupply.addn(1);

        it("emits an approval event", async function () {
          const { logs } = await this.token.increaseAllowance(spender, amount, {
            from: initialHolder,
          });

          expectEvent.inLogs(logs, "Approval", {
            owner: initialHolder,
            spender: spender,
            value: amount,
          });
        });

        describe("when there was no approved amount before", function () {
          it("approves the requested amount", async function () {
            await this.token.increaseAllowance(spender, amount, {
              from: initialHolder,
            });

            expect(
              await this.token.allowance(initialHolder, spender)
            ).to.be.bignumber.equal(amount);
          });
        });

        describe("when the spender had an approved amount", function () {
          beforeEach(async function () {
            await this.token.approve(spender, new BN(1), {
              from: initialHolder,
            });
          });

          it("increases the spender allowance adding the requested amount", async function () {
            await this.token.increaseAllowance(spender, amount, {
              from: initialHolder,
            });

            expect(
              await this.token.allowance(initialHolder, spender)
            ).to.be.bignumber.equal(amount.addn(1));
          });
        });
      });
    });

    describe("when the spender is the zero address", function () {
      const spender = ZERO_ADDRESS;

      it("reverts", async function () {
        await expectRevert(
          this.token.increaseAllowance(spender, amount, {
            from: initialHolder,
          }),
          "Approve to the zero address -- Reason given: Approve to the zero address."
        );
      });
    });
  });
  describe("pausing", function () {
    it("deployer can pause", async function () {
      const receipt = await this.token.pause({ from: deployer });
      expectEvent(receipt, "Paused", { account: deployer });

      expect(await this.token.paused()).to.equal(true);
    });

    it("deployer can unpause", async function () {
      await this.token.pause({ from: deployer });

      const receipt = await this.token.unpause({ from: deployer });
      expectEvent(receipt, "Unpaused", { account: deployer });

      expect(await this.token.paused()).to.equal(false);
    });

    it("cannot transfer while paused", async function () {
      await this.token.pause({ from: deployer });

      await expectRevert(
        this.token.transfer(anotherAccount, initialSupply, {
          from: deployer,
        }),
        "Token transfer while paused"
      );
    });

    it("other accounts cannot pause", async function () {
      await expectRevert(
        this.token.pause({ from: anotherAccount }),
        "Ownable: caller is not the owner -- Reason given: Ownable: caller is not the owner."
      );
    });

    it("other accounts cannot unpause", async function () {
      await this.token.pause({ from: deployer });

      await expectRevert(
        this.token.unpause({ from: anotherAccount }),
        "Ownable: caller is not the owner -- Reason given: Ownable: caller is not the owner."
      );
    });
  });

  shouldBehaveLikeBEP20(
    "BEP20",
    initialSupply,
    deployer,
    recipient,
    anotherAccount
  );

  describe("addBlackList", function () {
    it("deployer can add black list", async function () {
      const blackList = await this.token.addBlackList(anotherAccount, {
        from: deployer,
      });
      expectEvent(blackList, "AddedBlackList", { user: anotherAccount });
      await expectRevert(
        this.token.transfer(anotherAccount, initialSupply, {
          from: deployer,
        }),
        "Token transfer refused. Receiver is on blacklist -- Reason given: Token transfer refused. Receiver is on blacklist."
      );
    });

    it("deployer can remove black list", async function () {
      await this.token.addBlackList(anotherAccount, { from: deployer });

      const blackList = await this.token.removeBlackList(anotherAccount, {
        from: deployer,
      });
      expectEvent(blackList, "RemovedBlackList", { user: anotherAccount });
    });

    it("cannot transfer while paused", async function () {
      const blackList = await this.token.addBlackList(anotherAccount, {
        from: deployer,
      });

      await expectRevert(
        this.token.transfer(anotherAccount, initialSupply, {
          from: deployer,
        }),
        "Token transfer refused. Receiver is on blacklist -- Reason given: Token transfer refused. Receiver is on blacklist."
      );
    });

    it("other accounts cannot add blacklist", async function () {
      await expectRevert(
        this.token.addBlackList(recipient, { from: anotherAccount }),
        "Ownable: caller is not the owner -- Reason given: Ownable: caller is not the owner."
      );
    });

    it("other accounts cannot remove blacklist", async function () {
      await this.token.addBlackList(recipient, { from: deployer });

      await expectRevert(
        this.token.removeBlackList(recipient, { from: anotherAccount }),
        "Ownable: caller is not the owner -- Reason given: Ownable: caller is not the owner."
      );
    });
  });
});
