const {
  BN,
  constants,
  expectEvent,
  expectRevert,
} = require("@openzeppelin/test-helpers");

const { expect } = require("chai");

const {
  increaseTime,
  duration,
  increaseTimeTo,
  latestTime,
} = require("./utils/adjustTime");
const BEP20Token = artifacts.require("../MeyToken");
const TokenVesting = artifacts.require("../VestingVault");
const name = "Token";
const symbol = "TKN";
const initialSupply = new BN("2300000000000000000");
const decimals = "5";
contract("TokenVesting", function ([_, owner, beneficiary, anotherAccount]) {
  const amount = new BN(1000000000000);

  beforeEach(async function () {
    this.token = await BEP20Token.new(owner, {
      from: owner,
    });
    this.latestBlock = await web3.eth.getBlock("latest");
    this.start = await this.latestBlock.timestamp;
    this.cliff = 1000;
    this.duration = 2000;
    this.vesting = await TokenVesting.new(this.token.address, { from: owner });
    this.token.increaseAllowance(this.vesting.address, amount, { from: owner });
    this.vesting.addTokenGrant(
      beneficiary,
      this.start,
      amount,
      this.duration,
      this.cliff,
      { from: owner }
    );
  });
  it("could not add grant by another account", async function () {
    await expectRevert(
      this.vesting.addTokenGrant(
        beneficiary,
        this.start,
        amount,
        this.duration,
        this.cliff,
        { from: anotherAccount }
      ),
      "Ownable: caller is not the owner -- Reason given: Ownable: caller is not the owner."
    );
  });
  it("cannot be claim before cliff", async function () {
    await expectRevert(
      this.vesting.claimVestedTokens(0, { from: owner }),
      `VestingVault: Amount vested is zero`
    );
  });

  it("can be released after cliff", async function () {
    await increaseTimeTo(
      this.start + duration.days(this.cliff) + duration.weeks(1)
    );
    const { logs } = await this.vesting.claimVestedTokens(0, { from: owner });
    expectEvent.inLogs(logs, "GrantTokensClaimed", {});
  });

  it("should release proper amount after cliff", async function () {
    await increaseTimeTo(this.start + duration.days(this.cliff));
    const result = await this.vesting.calculateGrantClaim(0, { from: owner });

    const { 0: daysVest, 1: amountVest } = result;
    const { receipt } = await this.vesting.claimVestedTokens(0, {
      from: owner,
    });
    const releaseBlock = await latestTime();
    const releaseTime = releaseBlock.timestamp;
    var expectedBalance = amount
      .mul(new BN(releaseTime - this.start))
      .div(new BN(duration.days(this.duration)));
    const beneficiaryBalance = await this.token.balanceOf(beneficiary);
    expect(beneficiaryBalance.sub(expectedBalance)).to.be.bignumber.lte(
      new BN(10000)
    );
    expect(amountVest.sub(expectedBalance)).to.be.bignumber.lte(new BN(10000));
  });

  it("should linearly release tokens during vesting period", async function () {
    const vestingPeriod =
      duration.days(this.duration) - duration.days(this.cliff);
    const checkpoints = 4;

    for (let i = 1; i <= checkpoints; i++) {
      const now =
        this.start +
        duration.days(this.cliff) +
        i * (vestingPeriod / checkpoints);
      await increaseTimeTo(now);

      await this.vesting.claimVestedTokens(0, { from: owner });
      const expectedVesting = amount
        .mul(new BN(now - this.start))
        .div(new BN(duration.days(this.duration)));
      expect(await this.token.balanceOf(beneficiary)).to.be.bignumber.equal(
        expectedVesting
      );
    }
  });

  it("should have released all after end", async function () {
    await increaseTimeTo(this.start + duration.days(this.duration));
    await this.vesting.claimVestedTokens(0, { from: owner });
    expect(
      await this.token.balanceOf(beneficiary, { from: owner })
    ).to.be.bignumber.equal(amount);
  });

  it("could be remove by owner", async function () {
    expect(await this.vesting.removeTokenGrant(0, { from: owner }));
  });

  it("could not be remove by another account", async function () {
    await expectRevert(
      this.vesting.removeTokenGrant(0, { from: anotherAccount }),
      "Ownable: caller is not the owner -- Reason given: Ownable: caller is not the owner."
    );
  });

  it("should return the non-vested tokens when revoked by owner", async function () {
    await increaseTimeTo(
      this.start + duration.days(this.cliff) + duration.weeks(12)
    );

    const vested = await this.vesting.calculateGrantClaim(0, { from: owner });
    const { 0: daysVest, 1: amountVest } = vested;

    await this.vesting.removeTokenGrant(0, { from: owner });
    expect(await this.token.balanceOf(beneficiary)).to.be.bignumber.equal(
      amountVest
    );
    expect(await this.token.balanceOf(owner)).to.be.bignumber.equal(
      initialSupply.sub(amount).add(amount.sub(amountVest))
    );
  });
});
