const {
  BN,
  constants,
  expectEvent,
  expectRevert,
} = require("@openzeppelin/test-helpers");

const { expect } = require("chai");

const {
  increaseTime,
  duration,
  increaseTimeTo,
  latestTime,
} = require("./utils/adjustTime");
const BEP20Token = artifacts.require("../MeyToken");
const TokenSale = artifacts.require("../TokenSale");
const USDT = artifacts.require("../libs/Coin/BEP20USDT");
const name = "Token";
const symbol = "TKN";
const initialSupply = new BN("2300000000000000000");
const decimals = "9";
const transferRate = 25000000;
const vestPercentage = 92;
const vestDurationInDays = 12;
const vestCliffInDays = 0;
const buyAmount = new BN("1000000000000000000000000");
const initAmountSale = new BN("10000000000000000");

contract(
  "TokenSale",
  function ([_, owner, ownerUSDT, buyer, beneficiary, anotherAccount]) {
    const amount = new BN(1000000000000);

    beforeEach(async function () {
      this.token = await BEP20Token.new(owner, {
        from: owner,
      });
      this.tokenUSDT = await USDT.new({ from: ownerUSDT });
      this.latestBlock = await web3.eth.getBlock("latest");
      this.start = await this.latestBlock.timestamp;
      this.cliff = 30;
      this.duration = 3600;
      this.sale = await TokenSale.new(
        this.token.address,
        transferRate,
        vestPercentage,
        vestDurationInDays,
        vestCliffInDays,
        beneficiary,
        { from: owner }
      );
      this.sale.setSaleAmount(2500, 50000, {from: owner});
      this.tokenUSDT.transfer(buyer, buyAmount, { from: ownerUSDT });
    });
    describe("1. Update token support", function () {
      describe("should be able to add from owner", function () {
        it("add token support", async function () {
          expect(
            await this.sale.updateTokenSupport(this.tokenUSDT.address, true, {
              from: owner,
            })
          );
          expect(
            await this.sale.usdStableTokens(this.tokenUSDT.address, {
              from: owner,
            })
          ).equal(true);
        });
      });
      describe("should not be able to add from other account", function () {
        it("could not be remove by another account", async function () {
          await expectRevert(
            this.sale.updateTokenSupport(this.tokenUSDT.address, true, {
              from: anotherAccount,
            }),
            "Ownable: caller is not the owner -- Reason given: Ownable: caller is not the owner."
          );
          expect(
            await this.sale.usdStableTokens(this.tokenUSDT.address, {
              from: owner,
            })
          ).equal(false);
        });
      });
    });
    describe("2. Set unlock time", function () {
      describe("should be able to set from owner", function () {
        it("set unlock time successful", async function () {
          const releaseBlock = await latestTime();
          const releaseTime = releaseBlock.timestamp;
          expect(
            await this.sale.setUnlockTime(releaseTime, {
              from: owner,
            })
          );
          expect(
            await this.sale.unlockTime({
              from: owner,
            })
          ).to.be.bignumber.equal(new BN(releaseTime));
        });
      });
      describe("should not be able to set from other account", function () {
        it("set unlock time failed", async function () {
          const releaseBlock = await latestTime();
          const releaseTime = releaseBlock.timestamp;
          await expectRevert(
            this.sale.setUnlockTime(releaseTime, {
              from: anotherAccount,
            }),
            "Ownable: caller is not the owner -- Reason given: Ownable: caller is not the owner."
          );
          expect(
            await this.sale.unlockTime({
              from: owner,
            })
          ).to.be.bignumber.equal(new BN(0));
        });
      });
    });
    describe("3. Set sale period", function () {
      describe("should be able to set from owner", function () {
        it("set sale period successful", async function () {
          const releaseBlock = await latestTime();
          const releaseTime = releaseBlock.timestamp;
          expect(
            await this.sale.setSalePeriod(
              releaseTime + duration.days(1),
              releaseTime + duration.days(11),
              {
                from: owner,
              }
            )
          );
          expect(
            await this.sale.saleStartTime({
              from: owner,
            })
          ).to.be.bignumber.equal(new BN(releaseTime + duration.days(1)));
          expect(
            await this.sale.saleEndTime({
              from: owner,
            })
          ).to.be.bignumber.equal(new BN(releaseTime + duration.days(11)));
        });
        it("set sale period with end time earlier than start time", async function () {
          const releaseBlock = await latestTime();
          const releaseTime = releaseBlock.timestamp;
          await expectRevert(
            this.sale.setSalePeriod(
              releaseTime + duration.days(11),
              releaseTime + duration.days(1),
              {
                from: owner,
              }
            ),
            "TokenSale: endTime must be greater than startTime -- Reason given: TokenSale: endTime must be greater than startTime."
          );
        });
      });
      describe("should not be able to set from other account", function () {
        it("set sale period failed", async function () {
          const releaseBlock = await latestTime();
          const releaseTime = releaseBlock.timestamp;
          await expectRevert(
            this.sale.setSalePeriod(
              releaseTime + duration.days(1),
              releaseTime + duration.days(11),
              {
                from: anotherAccount,
              }
            ),
            "Ownable: caller is not the owner -- Reason given: Ownable: caller is not the owner."
          );
        });
      });
    });
    describe("4. Update white list", function () {
      describe("should be able to add from owner", function () {
        it("update white list successful", async function () {
          expect(
            await this.sale.updateWhiteList(anotherAccount, true, {
              from: owner,
            })
          );
        });
      });
      describe("should not be able to add from other account", function () {
        it("could not be remove by another account", async function () {
          await expectRevert(
            this.sale.updateWhiteList(anotherAccount, true, {
              from: anotherAccount,
            }),
            "Ownable: caller is not the owner -- Reason given: Ownable: caller is not the owner."
          );
        });
      });
    });
    describe("5. Add white list", function () {
      describe("should be able to add from owner", function () {
        it("add white list successful", async function () {
          expect(
            await this.sale.addWhiteList([anotherAccount], {
              from: owner,
            })
          );
        });
      });
      describe("should not be able to add from other account", function () {
        it("could not be add white list by another account", async function () {
          await expectRevert(
            this.sale.addWhiteList([anotherAccount], {
              from: anotherAccount,
            }),
            "Ownable: caller is not the owner -- Reason given: Ownable: caller is not the owner."
          );
        });
      });
    });
    describe("6. Buy token", function () {
      describe("fail when buying with invalid token input", function () {
        it("token not supported", async function () {
          await expectRevert(
            this.sale.buyToken(buyAmount, this.tokenUSDT.address, {
              from: buyer,
            }),
            "TokenSale: Invalid token address -- Reason given: TokenSale: Invalid token address."
          );
        });
      });
      describe("fail when buying with invalid amount input", function () {
        beforeEach(async function () {
          await this.sale.updateTokenSupport(this.tokenUSDT.address, true, {
            from: owner,
          });
        });
        it("amount too small", async function () {
          await expectRevert(
            this.sale.buyToken(
              new BN("99999999999999"),
              this.tokenUSDT.address,
              {
                from: buyer,
              }
            ),
            "TokenSale: Invalid input for amount -- Reason given: TokenSale: Invalid input for amount."
          );
        });
        it("amount too large", async function () {
          await expectRevert(
            this.sale.buyToken(
              new BN("2000000000000001"),
              this.tokenUSDT.address,
              {
                from: buyer,
              }
            ),
            "TokenSale: Invalid input for amount -- Reason given: TokenSale: Invalid input for amount."
          );
        });
      });
      describe("fail when buying user is not on whitelist", function () {
        beforeEach(async function () {
          await this.sale.updateTokenSupport(this.tokenUSDT.address, true, {
            from: owner,
          });
        });
        it("user not on white list", async function () {
          await expectRevert(
            this.sale.buyToken(
              new BN("100000000000000"),
              this.tokenUSDT.address,
              {
                from: buyer,
              }
            ),
            "TokenSale: Address is not on whitelist -- Reason given: TokenSale: Address is not on whitelist."
          );
          await expectRevert(
            this.sale.buyToken(
              new BN("2000000000000000"),
              this.tokenUSDT.address,
              {
                from: buyer,
              }
            ),
            "TokenSale: Address is not on whitelist -- Reason given: TokenSale: Address is not on whitelist."
          );
        });
      });
      describe("fail when buying time is not in time period", function () {
        beforeEach(async function () {
          await this.sale.updateTokenSupport(this.tokenUSDT.address, true, {
            from: owner,
          });
          await this.sale.updateWhiteList(buyer, true, {
            from: owner,
          });
        });
        it("time period not set", async function () {
          await expectRevert(
            this.sale.buyToken(
              new BN("100000000000000"),
              this.tokenUSDT.address,
              {
                from: buyer,
              }
            ),
            "TokenSale: Not in sale period -- Reason given: TokenSale: Not in sale period."
          );
        });
        it("not in time period ", async function () {
          const releaseBlock = await latestTime();
          const releaseTime = releaseBlock.timestamp;
          await this.sale.setSalePeriod(
            releaseTime + duration.days(1),
            releaseTime + duration.days(11),
            {
              from: owner,
            }
          );
          await expectRevert(
            this.sale.buyToken(
              new BN("100000000000000"),
              this.tokenUSDT.address,
              {
                from: buyer,
              }
            ),
            "TokenSale: Not in sale period -- Reason given: TokenSale: Not in sale period."
          );
        });
      });
      describe("fail when token sold out", function () {
        beforeEach(async function () {
          await this.sale.updateTokenSupport(this.tokenUSDT.address, true, {
            from: owner,
          });
          await this.sale.updateWhiteList(buyer, true, {
            from: owner,
          });
          const releaseBlock = await latestTime();
          const releaseTime = releaseBlock.timestamp;
          await this.sale.setSalePeriod(
            releaseTime - duration.days(1),
            releaseTime + duration.days(11),
            {
              from: owner,
            }
          );
          await this.tokenUSDT.approve(
            this.sale.address,
            new BN("3000000000000000000000000000000000"),
            {
              from: buyer,
            }
          );
        });
        it("not prepare enough token", async function () {
          await this.token.transfer(this.sale.address, new BN(1000000000000), {
            from: owner,
          });
          await expectRevert(
            this.sale.buyToken(
              new BN("100000000000000"),
              this.tokenUSDT.address,
              {
                from: buyer,
              }
            ),
            "TokenSale: Insufficient funds from contract -- Reason given: TokenSale: Insufficient funds from contract."
          );
        });
        it("token sold out", async function () {
          await this.token.transfer(
            this.sale.address,
            new BN(120000000000000),
            {
              from: owner,
            }
          );
          await expect(
            this.sale.buyToken(
              new BN("100000000000000"),
              this.tokenUSDT.address,
              {
                from: buyer,
              }
            )
          );
          await expectRevert(
            this.sale.buyToken(
              new BN("100000000000000"),
              this.tokenUSDT.address,
              {
                from: buyer,
              }
            ),
            "TokenSale: Insufficient funds from contract -- Reason given: TokenSale: Insufficient funds from contract."
          );
        });
      });
      describe("fail when user not approve USD", function () {
        beforeEach(async function () {
          await this.sale.updateTokenSupport(this.tokenUSDT.address, true, {
            from: owner,
          });
          await this.sale.updateWhiteList(buyer, true, {
            from: owner,
          });
          const releaseBlock = await latestTime();
          const releaseTime = releaseBlock.timestamp;
          await this.sale.setSalePeriod(
            releaseTime - duration.days(1),
            releaseTime + duration.days(11),
            {
              from: owner,
            }
          );

          await this.token.transfer(
            this.sale.address,
            new BN(100000000000000),
            {
              from: owner,
            }
          );
        });
        it("not approve usd to contract", async function () {
          await expectRevert(
            this.sale.buyToken(
              new BN("100000000000000"),
              this.tokenUSDT.address,
              {
                from: buyer,
              }
            ),
            "BEP20: transfer amount exceeds allowance -- Reason given: BEP20: transfer amount exceeds allowance."
          );
        });
        it("not approve enough usd to contract", async function () {
          await this.tokenUSDT.approve(
            this.sale.address,
            new BN("10000000000"),
            {
              from: buyer,
            }
          );
          await expectRevert(
            this.sale.buyToken(
              new BN("100000000000000"),
              this.tokenUSDT.address,
              {
                from: buyer,
              }
            ),
            "BEP20: transfer amount exceeds allowance -- Reason given: BEP20: transfer amount exceeds allowance."
          );
        });
      });
      describe("buy token successfully", function () {
        beforeEach(async function () {
          await this.sale.updateTokenSupport(this.tokenUSDT.address, true, {
            from: owner,
          });
          await this.sale.updateWhiteList(buyer, true, {
            from: owner,
          });
          const releaseBlock = await latestTime();
          const releaseTime = releaseBlock.timestamp;
          await this.sale.setSalePeriod(
            releaseTime - duration.days(1),
            releaseTime + duration.days(11),
            {
              from: owner,
            }
          );

          await this.token.transfer(
            this.sale.address,
            new BN(100000000000000),
            {
              from: owner,
            }
          );
          await this.tokenUSDT.approve(
            this.sale.address,
            new BN("10000000000000000000000"),
            {
              from: buyer,
            }
          );
        });
        it("buy success", async function () {
          const { logs } = await this.sale.buyToken(
            new BN("100000000000000"),
            this.tokenUSDT.address,
            {
              from: buyer,
            }
          );
          expectEvent.inLogs(logs, "BoughtOrder", {});
        });
      });
    });
    describe("7. Swap token", function () {
      describe("fail when token sold out", function () {
        beforeEach(async function () {
          await this.sale.updateTokenSupport(this.tokenUSDT.address, true, {
            from: owner,
          });
          await this.sale.updateWhiteList(buyer, true, {
            from: owner,
          });
          const releaseBlock = await latestTime();
          const releaseTime = releaseBlock.timestamp;
          await this.sale.setSalePeriod(
            releaseTime - duration.days(1),
            releaseTime + duration.days(11),
            {
              from: owner,
            }
          );
        });
        it("not prepare enough token", async function () {
          await this.token.transfer(this.sale.address, new BN(1000000000000), {
            from: owner,
          });
          await expectRevert(
            this.sale.swapTokenFromList([[new BN("2500000000000000000000"), buyer]],
              {
                from: owner,
              }
            ),
            "TokenSale: Insufficient funds from contract -- Reason given: TokenSale: Insufficient funds from contract."
          );
        });
        it("token sold out", async function () {
          await this.token.transfer(
            this.sale.address,
            new BN(120000000000000),
            {
              from: owner,
            }
          );
          await expect(
            this.sale.swapTokenFromList([[new BN("2500000000000000000000"), buyer]],
              {
                from: owner,
              }
            )
          );
          await expectRevert(
            this.sale.swapTokenFromList([[new BN("2500000000000000000000"), buyer]],
              {
                from: owner,
              }
            ),
            "TokenSale: Insufficient funds from contract -- Reason given: TokenSale: Insufficient funds from contract."
          );
        });
      }); 
      describe("swap token successfully", function () {
        beforeEach(async function () {
          await this.sale.updateTokenSupport(this.tokenUSDT.address, true, {
            from: owner,
          });
          await this.token.transfer(
            this.sale.address,
            new BN("10000000000000000"),
            {
              from: owner,
            }
          );
        });
        it("swap success", async function () {
          const { logs } = await this.sale.swapTokenFromList([[new BN("2500000000000000000000"), buyer]],
          {
            from: owner,
          }
        );
          expectEvent.inLogs(logs, "BoughtOrder", {});
          expect(await this.sale.availableAmount()).to.be.bignumber.equal(
            new BN("9900000000000000")
          );
        });
        it("swap multiple token success", async function () {
          const { logs } = await this.sale.swapTokenFromList([[new BN("2500000000000000000000"), buyer], [new BN("25000000000000000000000"), anotherAccount]],
          {
            from: owner,
          }
        );
          expectEvent.inLogs(logs, "BoughtOrder", {});
          expect(await this.sale.availableAmount()).to.be.bignumber.equal(
            new BN("8900000000000000")
          );
          expect((await this.sale.getTokenGrants(buyer))[0]).to.be.bignumber.equal(
            new BN("100000000000000")
          );
          expect((await this.sale.getTokenGrants(anotherAccount))[0]).to.be.bignumber.equal(
            new BN("1000000000000000")
          );
        });
      });  
    });
    describe("8. Available amount", function () {
      it("Display available amount", async function () {
        await this.token.transfer(this.sale.address, new BN(100000000000000), {
          from: owner,
        });
        expect(await this.sale.availableAmount()).to.be.bignumber.equal(
          new BN(100000000000000)
        );
      });
      describe("Display available amount after buy", async function () {
        beforeEach(async function () {
          await this.sale.updateTokenSupport(this.tokenUSDT.address, true, {
            from: owner,
          });
          await this.sale.updateWhiteList(buyer, true, {
            from: owner,
          });
          const releaseBlock = await latestTime();
          const releaseTime = releaseBlock.timestamp;
          await this.sale.setSalePeriod(
            releaseTime - duration.days(1),
            releaseTime + duration.days(11),
            {
              from: owner,
            }
          );

          await this.tokenUSDT.approve(
            this.sale.address,
            new BN("10000000000000000000000"),
            {
              from: buyer,
            }
          );
          await this.token.transfer(
            this.sale.address,
            new BN("300000000000000"),
            {
              from: owner,
            }
          );
          await this.sale.buyToken(
            new BN("100000000000000"),
            this.tokenUSDT.address,
            {
              from: buyer,
            }
          );
        });

        it("Display available amount reduced after token buy", async function () {
          expect(await this.sale.availableAmount()).to.be.bignumber.equal(
            new BN("200000000000000")
          );
          await this.sale.buyToken(
            new BN("200000000000000"),
            this.tokenUSDT.address,
            {
              from: buyer,
            }
          );
          await expectRevert(
            this.sale.buyToken(
              new BN("100000000000000"),
              this.tokenUSDT.address,
              {
                from: buyer,
              }
            ),
            "TokenSale: Insufficient funds from contract -- Reason given: TokenSale: Insufficient funds from contract."
          );
        });
      });
    });
    describe("9. Token vest per day", function () {
      it("Display token vest per day", async function () {
        await this.token.transfer(this.sale.address, new BN(100000000000000), {
          from: owner,
        });
        expect(await this.sale.tokensVestedPerMonth(buyer)).to.be.bignumber.equal(
          new BN(0)
        );
      });
      describe("Display token vest per day after buy", async function () {
        beforeEach(async function () {
          await this.sale.updateTokenSupport(this.tokenUSDT.address, true, {
            from: owner,
          });
          await this.sale.updateWhiteList(buyer, true, {
            from: owner,
          });
          const releaseBlock = await latestTime();
          const releaseTime = releaseBlock.timestamp;
          await this.sale.setSalePeriod(
            releaseTime - duration.days(1),
            releaseTime + duration.days(11),
            {
              from: owner,
            }
          );

          await this.tokenUSDT.approve(
            this.sale.address,
            new BN("10000000000000000000000"),
            {
              from: buyer,
            }
          );
          await this.token.transfer(
            this.sale.address,
            new BN("300000000000000"),
            {
              from: owner,
            }
          );
          await this.sale.buyToken(
            new BN("100000000000000"),
            this.tokenUSDT.address,
            {
              from: buyer,
            }
          );
        });

        it("display token vest after token buy", async function () {
          tokenVestPerdayExpected =
            (100000000000000 * (vestPercentage / 100)) / vestDurationInDays;
          expect(
            await this.sale.tokensVestedPerMonth(buyer)
          ).to.be.bignumber.equal(new BN(tokenVestPerdayExpected));
        });
      });
    });
    describe("10. Claim vested token", function () {
      describe("Claims vested token after buy", async function () {
        beforeEach(async function () {
          await this.sale.updateTokenSupport(this.tokenUSDT.address, true, {
            from: owner,
          });
          await this.sale.updateWhiteList(buyer, true, {
            from: owner,
          });
          const releaseBlock = await latestTime();
          const releaseTime = releaseBlock.timestamp;
          await this.sale.setSalePeriod(
            releaseTime - duration.days(1),
            releaseTime + duration.days(11),
            {
              from: owner,
            }
          );

          await this.tokenUSDT.approve(
            this.sale.address,
            new BN("10000000000000000000000"),
            {
              from: buyer,
            }
          );
          await this.token.transfer(
            this.sale.address,
            new BN("300000000000000"),
            {
              from: owner,
            }
          );
          await this.sale.buyToken(
            new BN("100000000000000"),
            this.tokenUSDT.address,
            {
              from: buyer,
            }
          );
        });

        it("claim token vest before unlockTime", async function () {
          const releaseBlock = await latestTime();
          const releaseTime = releaseBlock.timestamp;
          expect(
            await this.sale.setUnlockTime(releaseTime + 1000, {
              from: owner,
            })
          );

          tokenVestPerdayExpected =
            (100000000000000 * (vestPercentage / 100)) / vestDurationInDays;
          await expectRevert(
            this.sale.claimVestedTokens(buyer),
            "TokenSale: Amount vested is zero -- Reason given: TokenSale: Amount vested is zero."
          );
        });
        it("claim token vest after token unlockTime and before vesting cliff", async function () {
          const releaseBlock = await latestTime();
          const releaseTime = releaseBlock.timestamp;
          expect(
            await this.sale.setUnlockTime(releaseTime, {
              from: owner,
            })
          );

          tokenVestExpected = 100000000000000 * ((100 - vestPercentage) / 100);
          await expect(await this.sale.claimVestedTokens(buyer));
          expect(await this.token.balanceOf(buyer)).to.be.bignumber.equal(
            new BN(tokenVestExpected)
          );
          expect(await this.sale.availableAmount()).to.be.bignumber.equal(
            new BN(200000000000000)
          );
        });
        it("claim token vest after token unlockTime and before vesting cliff in second time", async function () {
          const releaseBlock = await latestTime();
          const releaseTime = releaseBlock.timestamp;
          expect(
            await this.sale.setUnlockTime(releaseTime, {
              from: owner,
            })
          );

          tokenVestExpected = 100000000000000 * ((100 - vestPercentage) / 100);
          await expect(await this.sale.claimVestedTokens(buyer));
          expect(await this.token.balanceOf(buyer)).to.be.bignumber.equal(
            new BN(tokenVestExpected)
          );
          await expectRevert(
            this.sale.claimVestedTokens(buyer),
            "TokenSale: Amount vested is zero -- Reason given: TokenSale: Amount vested is zero."
          );
        });
        it("claim token vest after token unlockTime and before vesting cliff every month", async function () {
          const releaseBlock = await latestTime();
          const releaseTime = releaseBlock.timestamp;
          expect(
            await this.sale.setUnlockTime(releaseTime, {
              from: owner,
            })
          );

          tokenVestExpected = 100000000000000 * ((100 - vestPercentage) / 100);
          await expect(await this.sale.claimVestedTokens(buyer));
          expect(await this.token.balanceOf(buyer)).to.be.bignumber.equal(
            new BN(tokenVestExpected)
          );
          tokenVestPerdayExpected =
            (100000000000000 * (vestPercentage / 100)) / vestDurationInDays;
          expect(
            await this.sale.tokensVestedPerMonth(buyer)
          ).to.be.bignumber.equal(new BN(tokenVestPerdayExpected));
          for (let i = 1; i < vestDurationInDays / 30; i++) {
            await increaseTimeTo(releaseTime + duration.days(30 * i));
            await expect(await this.sale.claimVestedTokens(buyer));
            tokenVestExpected +=
              30 *
              Math.floor(
                (100000000000000 * (vestPercentage / 100)) / vestDurationInDays
              );
            await expect(
              await this.token.balanceOf(buyer)
            ).to.be.bignumber.closeTo(new BN(tokenVestExpected), new BN(1));
            expect(await this.sale.availableAmount()).to.be.bignumber.equal(
              new BN(200000000000000)
            );
          }
        });
        it("claim token vest after end time", async function () {
          const releaseBlock = await latestTime();
          const releaseTime = releaseBlock.timestamp;
          expect(
            await this.sale.setUnlockTime(releaseTime, {
              from: owner,
            })
          );
          await increaseTimeTo(releaseTime + duration.days(360));
          await expect(await this.sale.claimVestedTokens(buyer));
          expect(await this.token.balanceOf(buyer)).to.be.bignumber.equal(
            new BN(100000000000000)
          );
          expect(await this.sale.availableAmount()).to.be.bignumber.equal(
            new BN(200000000000000)
          );
        });
        it("claim token vest one time before endtime and one time after end time", async function () {
          const releaseBlock = await latestTime();
          const releaseTime = releaseBlock.timestamp;
          expect(
            await this.sale.setUnlockTime(releaseTime, {
              from: owner,
            })
          );
          await increaseTimeTo(releaseTime + duration.days(180));
          await expect(await this.sale.claimVestedTokens(buyer));
          tokenVestPerdayExpected = Math.floor(
            (100000000000000 * (vestPercentage / 100)) / vestDurationInDays
          );
          tokenVestExpected =
            (100000000000000 * (100 - vestPercentage)) / 100 +
            tokenVestPerdayExpected * 6;
          expect(await this.token.balanceOf(buyer)).to.be.bignumber.equal(
            new BN(tokenVestExpected)
          );
          await increaseTimeTo(releaseTime + duration.days(360));
          await expect(await this.sale.claimVestedTokens(buyer));
          expect(await this.token.balanceOf(buyer)).to.be.bignumber.equal(
            new BN(100000000000000)
          );
          expect(await this.sale.availableAmount()).to.be.bignumber.equal(
            new BN(200000000000000)
          );
        });
      });
    });
    describe("11. Remove vest token", function () {
      describe("Owner remove token after buy", async function () {
        beforeEach(async function () {
          await this.sale.updateTokenSupport(this.tokenUSDT.address, true, {
            from: owner,
          });
          await this.sale.updateWhiteList(buyer, true, {
            from: owner,
          });
          const releaseBlock = await latestTime();
          const releaseTime = releaseBlock.timestamp;
          await this.sale.setSalePeriod(
            releaseTime - duration.days(1),
            releaseTime + duration.days(11),
            {
              from: owner,
            }
          );

          await this.tokenUSDT.approve(
            this.sale.address,
            new BN("10000000000000000000000"),
            {
              from: buyer,
            }
          );
          await this.token.transfer(
            this.sale.address,
            new BN("300000000000000"),
            {
              from: owner,
            }
          );
          await this.sale.buyToken(
            new BN("100000000000000"),
            this.tokenUSDT.address,
            {
              from: buyer,
            }
          );
        });
        it("remove token vest after end time", async function () {
          const releaseBlock = await latestTime();
          const releaseTime = releaseBlock.timestamp;
          expect(
            await this.sale.setUnlockTime(releaseTime, {
              from: owner,
            })
          );
          await increaseTimeTo(releaseTime + duration.days(360));
          await expect(
            await this.sale.removeTokenGrant(buyer, { from: owner })
          );
          expect(await this.token.balanceOf(buyer)).to.be.bignumber.equal(
            new BN(100000000000000)
          );
          expect(await this.sale.availableAmount()).to.be.bignumber.equal(
            new BN(200000000000000)
          );
        });
        it("remove token vest in between vesting time", async function () {
          const releaseBlock = await latestTime();
          const releaseTime = releaseBlock.timestamp;
          expect(
            await this.sale.setUnlockTime(releaseTime, {
              from: owner,
            })
          );
          await increaseTimeTo(releaseTime + duration.days(180));
          await expect(
            await this.sale.removeTokenGrant(buyer, { from: owner })
          );
          expect(await this.token.balanceOf(buyer)).to.be.bignumber.equal(
            new BN(100000000000000)
          );
          expect(await this.sale.availableAmount()).to.be.bignumber.equal(
            new BN(200000000000000)
          );
        });
        it("claim token vest before vesting time", async function () {
          const releaseBlock = await latestTime();
          const releaseTime = releaseBlock.timestamp;
          expect(
            await this.sale.setUnlockTime(releaseTime, {
              from: owner,
            })
          );
          await expect(
            await this.sale.removeTokenGrant(buyer, { from: owner })
          );
          expect(await this.token.balanceOf(buyer)).to.be.bignumber.equal(
            new BN(100000000000000)
          );
          expect(await this.sale.availableAmount()).to.be.bignumber.equal(
            new BN(200000000000000)
          );
        });
      });
      describe("Owner remove token without user buy", async function () {
        it("Owner remove token without user buy", async function () {
          await expectRevert(
            this.sale.removeTokenGrant(buyer, { from: owner })
          , "TokenSale: Invalid token grant -- Reason given: TokenSale: Invalid token grant.");
        });
      });
      describe("Other user remove token without user buy", async function () {
        it("Owner remove token without user buy", async function () {
          await expectRevert(
            this.sale.removeTokenGrant(buyer, { from: anotherAccount })
          , "Ownable: caller is not the owner -- Reason given: Ownable: caller is not the owner.");
        });
      });
    });
  }
);
