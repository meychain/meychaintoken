// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./libs/IBEP20.sol";
import "./libs/Ownable.sol";

contract TokenSender is Ownable {
    event TokenSend(address indexed recipient, uint256 amountSent);

    struct SendData {
        uint256 amount;
        address target;
    }

    function sendTokens(address token, SendData[] calldata arrayData)
        public
        onlyOwner
    {
        require(
            arrayData.length > 0,
            "TokenSender: Input array must not be empty"
        );
        //verify userBalance
        uint256 totalAmount = 0;
        uint256 contractBalance = IBEP20(token).balanceOf(address(this));
        for (uint256 i = 0; i < arrayData.length; i++) {
            totalAmount = totalAmount + arrayData[i].amount;
        }
        require(
            totalAmount <= contractBalance,
            "TokenSender: Insufficient funds"
        );
        for (uint256 i = 0; i < arrayData.length; i++) {
            IBEP20(token).transfer(arrayData[i].target, arrayData[i].amount);
            emit TokenSend(arrayData[i].target, arrayData[i].amount);
        }
    }

    function balanceOf(address token) public view returns (uint256) {
        return IBEP20(token).balanceOf(address(this));
    }
}
