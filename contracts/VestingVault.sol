// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./libs/Ownable.sol";
import "./libs/IBEP20.sol";

contract VestingVault is Ownable {
    modifier onlyValidAddress(address _recipient) {
        require(
            _recipient != address(0) &&
                _recipient != address(this) &&
                _recipient != address(token),
            "not valid _recipient"
        );
        _;
    }

    uint256 internal constant SECONDS_PER_DAY = 86400;

    struct Grant {
        uint256 startTime;
        uint256 amount;
        uint16 vestingDuration;
        uint16 vestingCliff;
        uint16 daysClaimed;
        uint256 totalClaimed;
        address recipient;
    }

    event GrantAdded(address indexed recipient, uint256 vestingId);
    event GrantTokensClaimed(address indexed recipient, uint256 amountClaimed);
    event GrantRemoved(
        address recipient,
        uint256 amountVested,
        uint256 amountNotVested
    );

    address public token;

    mapping(uint256 => Grant) public tokenGrants;
    mapping(address => uint256[]) private activeGrants;
    uint256 public totalVestingCount;

    constructor(address _token) {
        require(_token != address(0));
        token = _token;
    }

    function addTokenGrant(
        address _recipient,
        uint256 _startTime,
        uint256 _amount,
        uint16 _vestingDurationInDays,
        uint16 _vestingCliffInDays
    ) external onlyOwner {
        require(
            _vestingDurationInDays >= _vestingCliffInDays,
            "VestingVault: Duration must be greater than Cliff"
        );

        uint256 amountVestedPerDay = _amount / (_vestingDurationInDays);
        require(
            amountVestedPerDay > 0,
            "VestingVault: Amount vested per day must not be 0"
        );

        // Transfer the grant tokens under the control of the vesting contract
        require(
            IBEP20(token).transferFrom(
                address(msg.sender),
                address(this),
                _amount
            ),
            "transfer failed"
        );

        Grant memory grant = Grant({
            startTime: _startTime == 0 ? currentTime() : _startTime,
            amount: _amount,
            vestingDuration: _vestingDurationInDays,
            vestingCliff: _vestingCliffInDays,
            daysClaimed: 0,
            totalClaimed: 0,
            recipient: _recipient
        });
        tokenGrants[totalVestingCount] = grant;
        activeGrants[_recipient].push(totalVestingCount);
        emit GrantAdded(_recipient, totalVestingCount);
        totalVestingCount++;
    }

    function getActiveGrants(address _recipient)
        public
        view
        returns (uint256[] memory)
    {
        return activeGrants[_recipient];
    }

    /// @notice Calculate the vested and unclaimed months and tokens available for `_grantId` to claim
    /// Due to rounding errors once grant duration is reached, returns the entire left grant amount
    /// Returns (0, 0) if cliff has not been reached
    function calculateGrantClaim(uint256 _grantId)
        public
        view
        returns (uint16, uint256)
    {
        Grant storage tokenGrant = tokenGrants[_grantId];

        // For grants created with a future start date, that hasn't been reached, return 0, 0
        if (currentTime() < tokenGrant.startTime) {
            return (0, 0);
        }

        // Check cliff was reached
        uint256 elapsedTime = currentTime() - (tokenGrant.startTime);
        uint256 elapsedDays = elapsedTime / (SECONDS_PER_DAY);

        if (elapsedDays < tokenGrant.vestingCliff) {
            return (uint16(elapsedDays), 0);
        }

        // If over vesting duration, all tokens vested
        if (elapsedDays >= tokenGrant.vestingDuration) {
            uint256 remainingGrant = tokenGrant.amount -
                tokenGrant.totalClaimed;
            return (tokenGrant.vestingDuration, remainingGrant);
        } else {
            uint16 daysVested = uint16(elapsedDays - tokenGrant.daysClaimed);
            uint256 amountVestedPerDay = tokenGrant.amount /
                uint256(tokenGrant.vestingDuration);
            uint256 amountVested = uint256(daysVested * amountVestedPerDay);
            return (daysVested, amountVested);
        }
    }

    /// @notice Allows a grant recipient to claim their vested tokens. Errors if no tokens have vested
    /// It is advised recipients check they are entitled to claim via `calculateGrantClaim` before calling this
    function claimVestedTokens(uint256 _grantId) external {
        uint16 daysVested;
        uint256 amountVested;
        (daysVested, amountVested) = calculateGrantClaim(_grantId);
        require(amountVested > 0, "VestingVault: Amount vested is zero");

        Grant storage tokenGrant = tokenGrants[_grantId];
        tokenGrant.daysClaimed = uint16(tokenGrant.daysClaimed + daysVested);
        tokenGrant.totalClaimed = uint256(
            tokenGrant.totalClaimed + amountVested
        );

        require(
            IBEP20(token).transfer(tokenGrant.recipient, amountVested),
            "no tokens"
        );

        emit GrantTokensClaimed(tokenGrant.recipient, amountVested);
    }

    function removeTokenGrant(uint256 _grantId) external onlyOwner {
        Grant storage tokenGrant = tokenGrants[_grantId];
        address recipient = tokenGrant.recipient;
        uint16 daysVested;
        uint256 amountVested;
        (daysVested, amountVested) = calculateGrantClaim(_grantId);

        uint256 amountNotVested = (tokenGrant.amount -
            tokenGrant.totalClaimed -
            amountVested);

        require(IBEP20(token).transfer(recipient, amountVested));
        require(IBEP20(token).transfer(address(msg.sender), amountNotVested));

        tokenGrant.startTime = 0;
        tokenGrant.amount = 0;
        tokenGrant.vestingDuration = 0;
        tokenGrant.vestingCliff = 0;
        tokenGrant.daysClaimed = 0;
        tokenGrant.totalClaimed = 0;
        tokenGrant.recipient = address(0);

        emit GrantRemoved(recipient, amountVested, amountNotVested);
    }

    function currentTime() private view returns (uint256) {
        return block.timestamp;
    }

    function tokensVestedPerDay(uint256 _grantId)
        public
        view
        returns (uint256)
    {
        Grant storage tokenGrant = tokenGrants[_grantId];
        return tokenGrant.amount / uint256(tokenGrant.vestingDuration);
    }
}
