// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "./libs/IBEP20.sol";
import "./libs/Context.sol";
import "./libs/Ownable.sol";
import "./libs/Pausable.sol";

contract MeyToken is Context, IBEP20, Ownable, Pausable {
    mapping(address => uint256) private _balances;
    mapping(address => mapping(address => uint256)) private _allowances;
    uint256 internal _totalSupply;
    uint8 internal _decimals;
    string internal _symbol;
    string internal _name;
    mapping(address => bool) _blacklist;
    event AddedBlackList(address indexed user);
    event RemovedBlackList(address indexed user);
    event SeizeBlackFund(address indexed _blackListedUser, uint256 _balance);
    address _adminAddress;

    constructor(address adminAddress_) {
        _name = "MEY Token";
        _symbol = "MEY";
        _decimals = 9;
        _totalSupply = 2300000000000000000;
        _balances[msg.sender] = _totalSupply;
        _adminAddress = adminAddress_;
    }

    function getOwner() external view override returns (address) {
        return owner();
    }

    function decimals() external view override returns (uint8) {
        return _decimals;
    }

    function symbol() external view override returns (string memory) {
        return _symbol;
    }

    function name() external view override returns (string memory) {
        return _name;
    }

    function totalSupply() public view virtual override returns (uint256) {
        return _totalSupply;
    }

    function balanceOf(address account)
        public
        view
        virtual
        override
        returns (uint256)
    {
        return _balances[account];
    }

    function transfer(address recipient, uint256 amount)
        public
        virtual
        override
        returns (bool)
    {
        _transfer(_msgSender(), recipient, amount);
        return true;
    }

    function allowance(address owner, address spender)
        public
        view
        virtual
        override
        returns (uint256)
    {
        return _allowances[owner][spender];
    }

    function approve(address spender, uint256 amount)
        public
        virtual
        override
        returns (bool)
    {
        _approve(_msgSender(), spender, amount);
        return true;
    }

    function transferFrom(
        address sender,
        address recipient,
        uint256 amount
    ) public virtual override returns (bool) {
        _transfer(sender, recipient, amount);

        uint256 currentAllowance = _allowances[sender][_msgSender()];
        require(
            currentAllowance >= amount,
            "Transfer amount exceeds allowance"
        );
        unchecked {
            _approve(sender, _msgSender(), currentAllowance - amount);
        }

        return true;
    }

    function increaseAllowance(address spender, uint256 addedValue)
        public
        virtual
        returns (bool)
    {
        _approve(
            _msgSender(),
            spender,
            _allowances[_msgSender()][spender] + addedValue
        );
        return true;
    }

    function decreaseAllowance(address spender, uint256 subtractedValue)
        public
        virtual
        returns (bool)
    {
        uint256 currentAllowance = _allowances[_msgSender()][spender];
        require(
            currentAllowance >= subtractedValue,
            "Decreased allowance below zero"
        );
        unchecked {
            _approve(_msgSender(), spender, currentAllowance - subtractedValue);
        }

        return true;
    }

    function _transfer(
        address sender,
        address recipient,
        uint256 amount
    ) internal virtual {
        require(sender != address(0), "Transfer from the zero address");
        require(recipient != address(0), "Transfer to the zero address");
        require(
            !_blacklist[recipient],
            "Token transfer refused. Receiver is on blacklist"
        );
        require(
            !_blacklist[sender],
            "Token transfer refused. Receiver is on blacklist"
        );
        require(!paused(), "Token transfer while paused");
        _beforeTokenTransfer(sender, recipient, amount);

        uint256 senderBalance = _balances[sender];
        require(senderBalance >= amount, "Transfer amount exceeds balance");
        unchecked {
            _balances[sender] = senderBalance - amount;
        }
        _balances[recipient] += amount;

        emit Transfer(sender, recipient, amount);

        _afterTokenTransfer(sender, recipient, amount);
    }

    function _approve(
        address owner,
        address spender,
        uint256 amount
    ) internal virtual {
        require(owner != address(0), "Approve from the zero address");
        require(spender != address(0), "Approve to the zero address");

        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }

    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual {}

    function _afterTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual {}

    function addBlackList(address _blackListUser) external onlyOwner {
        _blacklist[_blackListUser] = true;
        emit AddedBlackList(_blackListUser);
    }

    function removeBlackList(address _clearedUser) external onlyOwner {
        _blacklist[_clearedUser] = false;
        emit RemovedBlackList(_clearedUser);
    }

    function seizeBlackFunds(address _blackListedUser) external onlyOwner {
        require(
            _blacklist[_blackListedUser],
            "MEY Token: address is not black list"
        );
        uint256 blackFund = balanceOf(_blackListedUser);
        _balances[_blackListedUser] = 0;
        _balances[_adminAddress] = _balances[_adminAddress] + blackFund;
        emit SeizeBlackFund(_blackListedUser, blackFund);
    }

    function adminAddress() public view returns (address) {
        return _adminAddress;
    }

    function setAdmin(address admin) external onlyOwner {
        _adminAddress = admin;
    }

    function pause() public onlyOwner {
        _pause();
    }

    function unpause() public onlyOwner {
        _unpause();
    }
}
