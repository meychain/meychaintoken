// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "../libs/Ownable.sol";

contract OwnableMock is Ownable {}