// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./libs/Ownable.sol";
import "./libs/IBEP20.sol";
import "./libs/IERC20.sol";

contract Timelock is Ownable {
    event TokenClaimed(uint256 amount);
    event TokenLocked(uint256 amount);
    address beneficiary;
    uint256 lockTime;
    
    constructor(
        uint256 _lockTime,
        address _beneficiary
    ) {
        require(_beneficiary != address(0), "LockToken: Invalid beneficiary address");
        lockTime = _lockTime;
        beneficiary = _beneficiary;
    }

    function deposit(address token, uint256 amount) external {
        IERC20(token).transferFrom(msg.sender, address(this), amount);
        emit TokenLocked(amount);
    }

    function withdraw(address token, uint256 amount) onlyOwner external {
        require(currentTime() >= lockTime, "LockToken: Withdraw too early");
        IERC20(token).transfer(beneficiary, amount);
        emit TokenClaimed(amount);
    }

    function currentTime() private view returns (uint256) {
        return block.timestamp;
    }
    function increaseTimeLock(uint256 timeIncrease) onlyOwner external {
        lockTime += timeIncrease;
    }
}
